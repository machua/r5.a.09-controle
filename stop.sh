#!/bin/bash

# On va lancer un a un les docker compose down
# Treafik
cd Traefik
docker-compose down
cd ../

# Loki
cd Loki
docker-compose down
cd ../

# Joomla
cd Joomla
docker-compose down
cd ../

# Wordpress
cd Wordpress
docker-compose down
cd ../

# Gitea
cd Gitea
docker-compose down
cd ../

# Minio
cd Minio
docker-compose down
cd ../

# MariadDB
cd MariadDB
docker-compose down
cd ../