
# Contrôle de virtualisation avancée


L'objectif de ce contrôle était de construire une infrastructure composee de plusieurs stacks.

Dans mon cas je devais utiliser :
- Traefik
- Loki (Ainsi que Grafana pour le faire marcher)
- Joomla
- Wordpress
- Gitea
- Minio

Je devais aussi créer une base de donnée accessible uniquement par Joomla et Gitea.
## Deploiment

Pour lancer le projet utilisez la commande suivante au niveau du dossier récupéré :

```bash
  ./init/sh
```

Pour stopper les stacks :
```bash
  ./stop.sh
```

