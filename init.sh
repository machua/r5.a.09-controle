#!/bin/bash

# On va lancer un a un les docker compose up
# Treafik
cd Traefik
docker network create traefik
docker-compose up -d --build
cd ../

# Loki
cd Loki
docker network create loki
docker-compose up -d --build
cd ../

# Joomla
cd Joomla
docker network create joomla
docker-compose up -d --build
cd ../

# Wordpress
cd Wordpress
docker network create wordpress
docker-compose up -d --build
cd ../

# Gitea
cd Gitea
docker network create gitea
docker-compose up -d --build
cd ../

# Minio
cd Minio
docker network create minio
docker-compose up -d --build
cd ../

# MariaDB
cd MariaDB
docker network create externaldb
docker-compose up -d --build
cd ../